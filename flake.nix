

{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";

    devenv.url = "github:cachix/devenv";
  };

  nixConfig = {
    extra-trusted-public-keys = "devenv.cachix.org-1:w1cLUi8dv3hnoSPGAuibQv+f9TZLr6cv/Hm9XgU50cw=";
    extra-substituters = "https://devenv.cachix.org";
  };

  outputs = inputs @ {self, ...}:
    with inputs;
      flake-utils.lib.eachDefaultSystem (
        system: let
          pkgs = import nixpkgs {inherit system;};
        in {
          packages.${system}.devenv-up = self.devShells.${system}.default.config.procfileScript;

          devShells.default = devenv.lib.mkShell {
            inherit inputs pkgs;
            modules = [
              ({
                pkgs,
                config,
                ...
              }: {
                packages = with pkgs; [nodePackages.vercel prisma-engines];

                # Enable NodeJS
                languages.javascript = {
                  enable = true;
                  package = pkgs.nodejs_20;

                  # Enable `yarn`
                  yarn = {
                    enable = true;
                    install.enable = true;
                  };
                };
                
                dotenv.enable = false;

                env = with pkgs; {
                  PRISMA_SCHEMA_ENGINE_BINARY = "${prisma-engines}/bin/schema-engine";
                  PRISMA_QUERY_ENGINE_BINARY = "${prisma-engines}/bin/query-engine";
                  PRISMA_QUERY_ENGINE_LIBRARY = "${prisma-engines}/lib/libquery_engine.node";
                  PRISMA_INTROSPECTION_ENGINE_BINARY = "${prisma-engines}/bin/introspection-engine";
                  PRISMA_FMT_BINARY = "${prisma-engines}/bin/prisma-fmt";
                  PATH = "$PWD/node_modules/.bin/:$PATH";
                };
              })
            ];
          };
        }
      );
}

