import { defineConfig } from 'astro/config';
import tailwind from "@astrojs/tailwind";
import sitemap from "@astrojs/sitemap";
import remarkFigureCaption from "@microflash/remark-figure-caption";
import rehypeAutolinkHeadings from "rehype-autolink-headings";
import mdx from "@astrojs/mdx";

import vercel from "@astrojs/vercel/serverless";

import dsv from '@rollup/plugin-dsv';

// https://astro.build/config
export default defineConfig({
  integrations: [tailwind(), sitemap(), mdx()],
  site: "https://wale.au",
  markdown: {
    shikiConfig: {
      theme: "css-variables",
      wrap: true
    },
    remarkPlugins: [remarkFigureCaption],
    rehypePlugins: [[rehypeAutolinkHeadings, {
      behavior: "append",
      content: {
        type: "text",
        value: "#"
      }
    }]]
  },
  vite: {
    plugins: [dsv()]
  },
  output: "hybrid",
  adapter: vercel()
});