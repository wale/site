---
title: "Now"
description: "What is Wale up to now?"
published: 2024-04-27T10:50:16+10:00
---

*This is a [/now](https://nownownow.com/about) page - the concept was coined by [Derek Sivers](https://sive.rs). You should check it out!*

## University Crunch
I am currently approaching the end of the first semester at my university, with about four weeks left as of writing. While the crunch was not easy this time around, I may be approaching the light at the end of the tunnel.

## Open Source
Some of what I have left of my free time tends to go to developing open-source websites and software applications. My current focuses are:
- [Readconquista](https://codeberg.org/wale/readconquista), a web-app for a library card collection.
- *TBA*, a self-managed application for photographers to manage image storage.

I'm also in the process of slowly migrating my repositories to the [Codeberg](https://codeberg.org) service, which has proven to be a (mostly) smooth migration.

## Photography
As always, I've been doing solo [photowalks](https://en.wikipedia.org/wiki/Photowalking) around the Greater Melbourne area, and some of the photos have turned out pretty well for my amateur-ish skill.

![A photograph of a [X'Trapolis 100](https://en.wikipedia.org/wiki/X%27Trapolis_100) train over the Flinders Street overpass.](https://cdn.wale.au/i/IMG_4844.jpg)

![A HCMT train stopped at South Yarra station with a reflection of a building on its front window.](https://cdn.wale.au/i/IMG_4828.jpg)

## Music
I've mostly been listening to a lot of drum-and-bass and electronic music, mainly due to it being a genre I was fond of for a while. Doesn't help that [Chase & Status](https://en.wikipedia.org/wiki/Chase_%26_Status) are touring Australia.

My notable picks include:
- [Venjent](https://www.youtube.com/@Venjent)
- [Bou](https://www.youtube.com/@bou_dnb)
- [Plago](https://soundcloud.com/plagodnb)
- [FractalOne](https://soundcloud.com/fractalone)