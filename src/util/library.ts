export type Library = {
    service: string;
    lga?: string[] | undefined;
    date: Date;
    note: string;
    collectedAt: string;
    isCaval: boolean;
}

// Greater Melbourne only right now.
export const libraryServices = [
    "Yarra Plenty Regional Library",
    "City of Melbourne Libraries",
    "Darebin Libraries",
    "Merri-bek Library Service",
    "Boroondara Library Service",
    "Port Philip Library Service",
    "Stonnington Libraries",
    "Glen Eira Libraries",
    "Whitehorse Manningham Libraries",
    "Maribyrnong Library Service",
    "Monash Public Library Service",
    "Eastern Regional Libraries",
    "Moonee Valley Libraries",
    "Bayside Libraries",
    "Brimbank Libraries",
    "Hume Libraries",
    "Wyndham City Libraries",
    "Melton City Libraries",
    "My Community Libraries",
    "Connected Libraries",
    "Greater Dandenong Libraries",
    "Mornington Peninsula Libraries",
    "Frankston City Libraries",
    "Hobsons Bay Libraries",
    "Kingston Libraries",
    "Yarra Libraries"
];

export const cavalServices = [
    "Bendigo TAFE",
    "Box Hill Institute",
    "Chisholm Institute",
    "Deakin University",
    "Federation University Australia",
    "Gordon Institute of TAFE",
    "Goulburn Ovens Institute of TAFE",
    "Holmesglen",
    "Kangan Institute",
    "La Trobe University",
    "Melbourne Polytechnic",
    "Monash University",
    "RMIT University",
    "South West Institute of TAFE",
    "TAFE Gippsland",
    "The University of Melbourne",
    "Victoria University",
    "William Angliss Institute",
]